package app.single_point_access;

import app.service.*;
import app.service.implementation.*;
import app.service.performance.AppPerformanceService;
import app.service.performance.SQLProcedurePerformanceService;

public class ServiceSinglePointAccess {

    private static UserService userService;
    private static AuthorsService authorsService;
    private static GenresService genresService;
    private static BooksService booksService;
    private static BorrowedBooksService borrowedBooksService;
    private static AppPerformanceService appPerformanceService;
    private static SQLProcedurePerformanceService SQLProcedurePerformanceService;

    static {
        userService = new UserServiceImpl();
        authorsService = new AuthorsServiceImpl();
        genresService = new GenresServiceImpl();
        booksService = new BooksServiceImpl();
        borrowedBooksService = new BorrowedBooksServiceImpl();
        appPerformanceService = new AppPerformanceService();
        SQLProcedurePerformanceService = new SQLProcedurePerformanceService();
    }

    public static AppPerformanceService getAppPerformanceService() {
        return appPerformanceService;
    }

    public static app.service.performance.SQLProcedurePerformanceService getSQLProcedurePerformanceService() {
        return SQLProcedurePerformanceService;
    }

    public static UserService getUserService() {
        return userService;
    }

    public static AuthorsService getAuthorsService() {
        return authorsService;
    }

    public static GenresService getGenresService() {
        return genresService;
    }

    public static BooksService getBooksService() {
        return booksService;
    }

    public static BorrowedBooksService getBorrowedBooksService() {
        return borrowedBooksService;
    }

}