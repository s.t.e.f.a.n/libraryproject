package app.single_point_access;

import app.repository.*;
import app.repository.implemetation.*;

public class RepositorySinglePointAccess {

    private static UserRepository userRepository;

    private static AuthorsRepository authorsRepository;

    private static GenresRepository genresRepository;

    private static BooksRepository booksRepository;

    private static BorrowedBooksRepository borrowedBooksRepository;

    static {
        userRepository = new UserRepositoryImpl();
        authorsRepository = new AuthorsRepositoryImpl();
        genresRepository = new GenresRepositoryImpl();
        booksRepository = new BooksRepositoryImpl();
        borrowedBooksRepository = new BorrowedBooksRepositoryImpl();
    }

    public static UserRepository getUserRepository() {
        return userRepository;
    }

    public static AuthorsRepository getAuthorsRepository() {
        return authorsRepository;
    }

    public static GenresRepository getGenresRepository() {
        return genresRepository;
    }

    public static BooksRepository getBooksRepository() {
        return booksRepository;
    }

    public static BorrowedBooksRepository getBorrowedBooksRepository() {
        return borrowedBooksRepository;
    }

}