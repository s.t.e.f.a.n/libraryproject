package app.dto;

import app.model.Authors;
import lombok.Data;

@Data
public class BooksDTO {
    private Authors author;
    private String title;
}