package app.controller.rest;

import app.dto.csvDTO;
import app.model.User;
import app.service.UserService;
import app.single_point_access.ServiceSinglePointAccess;
import app.util.FileUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


// All imported/exported files are taken form resources directory ONLY
@RestController
@RequestMapping("/csv")
public class CSVController {

    private UserService userService = ServiceSinglePointAccess.getUserService();

    // TO DO
    // For project take in consideration that a csv file could have different order of columns
    // Do it for at least 2 entities - import and export
    // Take in consideration data validation or if some data already exists
    // Extract duplicate logic and improve it based on template below
    //
    // For demo - import at least 25 entities and export all entities
    //
    @PostMapping("/import_user")
    public ResponseEntity<Boolean> importUserFromCSV(@RequestBody String filename) {
        try {
            File file = FileUtil.getAndCreateFileFromResourcesDirectory(filename);

            // Read data in a buffer
            BufferedReader br = new BufferedReader(new FileReader(file));
            List<User> users = new ArrayList<>();
            String line;
            boolean firstLine = true;
            String[] h = null;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");

                if (firstLine) {
                    firstLine = false;
                    h = values;
                    continue;
                }

                User user = new User();

                // The order in csv could be changed
                // This implementation is only for template purpose
                //user.setName(values[0]);
                //user.setPassword(values[1]);
                //user.setPhone(values[2]);
                //user.setSalary(Integer.valueOf(values[3]));

                for (int i = 0; i < values.length; i++) {
                    if (i < h.length) {
                        switch (h[i].toLowerCase()) {
                            case "membername":
                                user.setMemberName(values[i]);
                                break;
                            case "email":
                                user.setEmail(values[i]);
                                break;
                            case "phone":
                                user.setPhone(values[i]);
                                break;
                            default:
                                break;
                        }
                    }
                }
                users.add(user);
            }

            for (User userIterator : users) {
                userService.save(userIterator);
            }

            return ResponseEntity.status(HttpStatus.OK).body(true);
        } catch (FileNotFoundException e) {
            return ResponseEntity.status(HttpStatus.OK).body(false);
            // TO DO - treat exception case
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(false);
            // TO DO - treat exception case
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
            // TO DO - treat exception case
        }
    }

    // You can send the order of fields that must appear in csv
    // Add a new parameter for header
    @PostMapping("/export_user")
    public ResponseEntity<Boolean> exportUserToCSV(@RequestBody csvDTO request) {

        try {
            File file = FileUtil.getAndCreateFileFromResourcesDirectory(request.getFileName());
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(request.getH());

            List<User> users = userService.findAll();
            for (User user1 : users) {
                StringBuilder member = new StringBuilder();
                String[] array = request.getH().split(",");
                for (String a : array) {
                    switch (a.trim()) {
                        case "membername":
                            member.append(user1.getMemberName()).append(",");
                            break;
                        case "email":
                            member.append(user1.getEmail()).append(",");
                            break;
                        case "phone":
                            member.append(user1.getPhone()).append(",");
                            break;
                    }
                }
                //member.deleteCharAt(member.length());
                member.append("\n");
                fileWriter.write(member.toString());
            }

            fileWriter.close();

            return ResponseEntity.status(HttpStatus.OK).body(true);
        } catch (IOException ex) {

            // TO DO - treat exception case
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(false);
        } catch (URISyntaxException e) {

            // TO DO - treat exception case
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
        }
    }
}