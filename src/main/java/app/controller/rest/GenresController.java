package app.controller.rest;

import app.model.Genres;
import app.service.GenresService;
import app.single_point_access.ServiceSinglePointAccess;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/genres")
public class GenresController {
    private GenresService genresService = ServiceSinglePointAccess.getGenresService();

    @GetMapping("/all")
    public ResponseEntity<List<Genres>> getAllGenres() {
        return ResponseEntity.status(HttpStatus.OK).body(genresService.findAll());
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Genres> getGenresById(@PathVariable Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(genresService.findById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Genres> createGenres(@RequestBody Genres genres) {
        return ResponseEntity.status(HttpStatus.OK).body(genresService.save(genres));
    }

    @PutMapping("/update")
    public ResponseEntity<Genres> update(@RequestBody Genres genres) {
        Genres genresFromDB = genresService.findById(genres.getId());
        genresFromDB.setGenreName(genres.getGenreName());
        Genres genresUpdated = genresService.update(genresFromDB);
        return ResponseEntity.status(HttpStatus.OK).body(genresUpdated);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteById(@RequestBody Integer id) {
        Genres genres = genresService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(genresService.delete(genres));
    }

    @GetMapping("/findByName/{name}")
    public ResponseEntity<Genres> findGenreByName(@PathVariable String name) {
        return ResponseEntity.status(HttpStatus.OK).body(genresService.findGenreByName(name));
    }

}