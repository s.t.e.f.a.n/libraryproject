package app.controller.rest;


import app.dto.BooksDTO;
import app.model.Books;
import app.service.AuthorsService;
import app.service.BooksService;
import app.service.GenresService;
import app.single_point_access.ServiceSinglePointAccess;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController

@RequestMapping("/books")
public class BooksController {
    private BooksService booksService = ServiceSinglePointAccess.getBooksService();
    private AuthorsService authorsService = ServiceSinglePointAccess.getAuthorsService();
    private GenresService genresService = ServiceSinglePointAccess.getGenresService();

    @GetMapping("/all")
    public ResponseEntity<List<Books>> getAllBooks() {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findAll());
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Books> getBookById(@PathVariable Integer id) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Books> createBook(@RequestBody Books books) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.save(books));
    }

    @PostMapping("/addBook/{title}/{authorName}/{gen}/{isbn}/{nrCopiiDisp}")
    public ResponseEntity<Books> addBook(@PathVariable String title, @PathVariable String authorName, @PathVariable String gen, @PathVariable String isbn, @PathVariable Integer nrCopiiDisp) {
        Books book = new Books();
        book.setTitle(title);
        book.setAuthor(authorsService.findAuthorByName(authorName));
        book.setGenre(genresService.findGenreByName(gen));
        book.setIsbn(isbn);
        book.setAvailableCopies(nrCopiiDisp);
        return ResponseEntity.status(HttpStatus.OK).body(booksService.save(book));
    }

    @PutMapping("/update")
    public ResponseEntity<Books> update(@RequestBody Books books) {
        Books bookFromDB = booksService.findById(books.getId());
        bookFromDB.setTitle(books.getTitle());
        bookFromDB.setIsbn(books.getIsbn());
        bookFromDB.setAvailableCopies(books.getAvailableCopies());
        bookFromDB.setAuthor(books.getAuthor());
        bookFromDB.setGenre(books.getGenre());
        Books bookUpdated = booksService.update(bookFromDB);
        return ResponseEntity.status(HttpStatus.OK).body(bookUpdated);
    }

    @Operation(summary = "Get details (author, title)")
    @GetMapping("/getGenresBook/{genres}")
    public ResponseEntity<List<BooksDTO>> getGenresBook(@PathVariable String genres) {
        List<Books> books = booksService.findAll();
        List<BooksDTO> getList = new ArrayList<>();
        for (Books book : books) {
            if (book.getGenre().getGenreName().equals(genres)) {
                BooksDTO dto = new BooksDTO();
                dto.setAuthor(book.getAuthor());
                dto.setTitle(book.getTitle());
                getList.add(dto);
            }
        }
        return ResponseEntity.status(HttpStatus.OK).body(getList);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Boolean> deleteById(@RequestBody Integer id) {
        Books books = booksService.findById(id);
        return ResponseEntity.status(HttpStatus.OK).body(booksService.delete(books));
    }

    @DeleteMapping("/removeBook/{title}/{authorName}")
    public ResponseEntity<Boolean> removeBook(@PathVariable String title, @PathVariable String authorName) {
        Books books = booksService.findBookByTitleAndAuthor(title, authorName);
        return ResponseEntity.status(HttpStatus.OK).body(booksService.delete(books));
    }

    @GetMapping("/findBookByTitle/{title}")
    public ResponseEntity<Books> findBookByTitle(@PathVariable String title) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findBookByTitle(title));
    }

    @GetMapping("/findBookByTitleAndAuthor/{title}/{authorName}")
    public ResponseEntity<Books> findBookByTitleAndAuthor(@PathVariable String title, @PathVariable String authorName) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.findBookByTitleAndAuthor(title, authorName));
    }

}