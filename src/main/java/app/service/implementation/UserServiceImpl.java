package app.service.implementation;

import app.model.Movie;
import app.model.Reservation;
import app.model.User;
import app.repository.MovieRepository;
import app.repository.UserRepository;
import app.service.UserService;
import app.single_point_access.RepositorySinglePointAccess;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository = RepositorySinglePointAccess.getUserRepository();
    //private MovieRepository movieRepository = RepositorySinglePointAccess.getMovieRepository();

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        return userRepository.update(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public boolean delete(User user) {
        return userRepository.delete(user);
    }

    @Override
    public User findByIdAndName(Integer id, String name) {
        return userRepository.findByIdAndName(id, name);
    }

}