package app.service.implementation;

import app.model.Genres;
import app.repository.GenresRepository;
import app.service.GenresService;
import app.single_point_access.RepositorySinglePointAccess;

import java.util.List;

public class GenresServiceImpl implements GenresService {
    private GenresRepository genresRepository = RepositorySinglePointAccess.getGenresRepository();

    @Override
    public Genres save(Genres genres) {
        return genresRepository.save(genres);
    }

    @Override
    public Genres update(Genres genres) {
        return genresRepository.update(genres);
    }

    @Override
    public List<Genres> findAll() {
        return genresRepository.findAll();
    }

    @Override
    public Genres findById(Integer id) {
        return genresRepository.findById(id);
    }

    @Override
    public boolean delete(Genres genres) {
        return genresRepository.delete(genres);
    }

    @Override
    public Genres findGenreByName(String name) {
        return genresRepository.findGenreByName(name);
    }

}