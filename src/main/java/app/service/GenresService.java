package app.service;

import app.model.Genres;

import java.util.List;

public interface GenresService {
    Genres save(Genres genres);

    Genres update(Genres genres);

    List<Genres> findAll();

    Genres findById(Integer id);

    boolean delete(Genres genres);

    Genres findGenreByName(String name);

}