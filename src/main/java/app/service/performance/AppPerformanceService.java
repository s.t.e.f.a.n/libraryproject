package app.service.performance;

import app.model.*;
import app.repository.MovieRepository;
import app.service.AuthorsService;
import app.service.BooksService;
import app.service.UserService;
import app.single_point_access.RepositorySinglePointAccess;
import app.single_point_access.ServiceSinglePointAccess;

import java.util.List;

public class AppPerformanceService implements PerformanceService {

    private BooksService booksService = ServiceSinglePointAccess.getBooksService();
    private AuthorsService authorsService = ServiceSinglePointAccess.getAuthorsService();

    @Override
    public void applyLogicOnUsers() {
        List<Authors> authors = authorsService.findAll();
        for (Authors authors1 : authors) {
            authors1.setName(authors1.getName().toUpperCase());
        }
        List<Books> books = booksService.findAll();
        for (Books books1 : books) {
            if (books1.getAvailableCopies() % 2 == 0) {
                books1.setAvailableCopies(books1.getAvailableCopies() + 5);
            }
        }
    }
}