package app.service;

import app.model.BorrowedBooks;

import java.util.Date;
import java.util.List;

public interface BorrowedBooksService {
    BorrowedBooks save(BorrowedBooks books);

    BorrowedBooks update(BorrowedBooks books);

    List<BorrowedBooks> findAll();

    BorrowedBooks findById(Integer id);

    boolean delete(BorrowedBooks books);

    BorrowedBooks findBorrowedBook(Integer memberId, String memberName, String title, String authorName);

    BorrowedBooks borrowBook(Integer memberId, String memberName, String title, String authorName, String borrowDate);

    BorrowedBooks returnBook(Integer memberId, String memberName, String title, String authorName, String returnDate);

}