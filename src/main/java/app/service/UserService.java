package app.service;

import app.model.Movie;
import app.model.User;

import java.util.Date;
import java.util.List;

public interface UserService {

    User save(User user);

    User update(User user);

    List<User> findAll();

    User findById(Integer id);

    boolean delete(User user);

    User findByIdAndName(Integer id, String name);

}