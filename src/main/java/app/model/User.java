package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        {
                @NamedQuery(name = "findMemberByName", query = "select a from User a where a.memberName=:memberName"),
                @NamedQuery(name = "findMemberById", query = "select a from User a where a.id=:id"),
                @NamedQuery(name = "findMemberByEmail", query = "select a from User a where a.email=:email"),
                @NamedQuery(name = "findMemberByPhone", query = "select a from User a where a.phone=:phone"),
                @NamedQuery(name = "findAllUsers", query = "select a from User a"),
                @NamedQuery(name = "findMemberByIdAndName", query = "select a from User a where a.id=:id and a.memberName=:memberName")
        }
)

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String memberName;

    @Column
    private String email;

    @Column
    private String phone;

}