package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        {
                @NamedQuery(name = "findBookByTitle", query = "select a from Books a where a.title=:title"),
                @NamedQuery(name = "findBookById", query = "select a from Books a where a.id=:id"),
                @NamedQuery(name = "findBookByIsbn", query = "select a from Books a where a.isbn=:isbn"),
                @NamedQuery(name = "findAllBooks", query = "select a from Books a"),
                @NamedQuery(name = "findBookByTitleAndAuthor", query = "select a from Books a where a.title=:title and a.author=:author")
        }
)

public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String title;

//    @Column
//    private Integer authorId;
//
//    @Column
//    private Integer genreId;

    @Column
    private String isbn;

    @Column
    private Integer availableCopies;

    @ManyToOne
    @JoinColumn(name = "Authors_id", referencedColumnName = "id")
    private Authors author;

    @ManyToOne
    @JoinColumn(name = "Genres_id", referencedColumnName = "id")
    private Genres genre;

}