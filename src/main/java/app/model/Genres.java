package app.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        {
                @NamedQuery(name = "findGenreByName", query = "select a from Genres a where a.genreName=:genreName"),
                @NamedQuery(name = "findGenreById", query = "select a from Genres a where a.id=:id"),
                @NamedQuery(name = "findAllGenres", query = "select a from Genres a")
        }
)
public class Genres {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String genreName;
}