package app.repository.implemetation;

import app.configuration.HibernateConfiguration;
import app.model.Authors;
import app.model.User;
import app.repository.AuthorsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.SQLException;
import java.util.List;

public class AuthorsRepositoryImpl implements AuthorsRepository {
    @Override
    public Authors save(Authors entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer idOnAuthorsSaved = (Integer) session.save(entity);

        transaction.commit();
        session.close();

        return findById(idOnAuthorsSaved);
    }

    @Override
    public Authors update(Authors entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.saveOrUpdate(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public boolean delete(Authors entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.delete(entity);

        transaction.commit();
        session.close();

        return findById(id) == null;
    }

    @Override
    public List<Authors> findAll() {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        // Native SQL - not preferred
//         Query query = session.createSQLQuery("select * from user");

        TypedQuery<Authors> query = session.getNamedQuery("findAllAuthors");
        List<Authors> authors = query.getResultList();

        transaction.commit();
        session.close();

        return authors;
    }

    @Override
    public Authors findById(Integer id) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from Authors where id=:id");
        query.setParameter("id", id);

        Authors authors;
        try {
            authors = (Authors) query.getSingleResult();
        } catch (NoResultException e) {
            authors = null;
        }

        transaction.commit();
        session.close();

        return authors;
    }

    @Override
    public Authors findAuthorByName(String name) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        TypedQuery<Authors> query = session.getNamedQuery("findAuthorByName");
        query.setParameter("name", name);
        Authors authors;
        try {
            authors = (Authors) query.getSingleResult();
        } catch (NoResultException e) {
            e.printStackTrace();
            authors = null;
        }
        transaction.commit();
        session.close();

        return authors;
    }
}