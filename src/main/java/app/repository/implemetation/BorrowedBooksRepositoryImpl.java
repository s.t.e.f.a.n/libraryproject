package app.repository.implemetation;

import app.configuration.HibernateConfiguration;
import app.model.*;
import app.repository.BorrowedBooksRepository;
import app.service.BooksService;
import app.service.UserService;
import app.service.implementation.BooksServiceImpl;
import app.service.implementation.UserServiceImpl;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class BorrowedBooksRepositoryImpl implements BorrowedBooksRepository {
    private BooksService booksService = new BooksServiceImpl();
    private UserService userService = new UserServiceImpl();

    @Override
    public BorrowedBooks save(BorrowedBooks entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public BorrowedBooks borrowBook(Integer memberId, String memberName, String title, String authorName, String borrowDate) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = (Integer) session.save(findBorrowedBook(memberId, memberName, title, authorName));

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public BorrowedBooks findBorrowedBook(Integer memberId, String memberName, String title, String authorName) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        TypedQuery<BorrowedBooks> query = session.getNamedQuery("findBorrowedBook");

        Books book = booksService.findBookByTitleAndAuthor(title, authorName);
        User user = userService.findByIdAndName(memberId, memberName);
        query.setParameter("book", book);
        query.setParameter("member", user);
        BorrowedBooks borrowedBooks;

        try {
            borrowedBooks = (BorrowedBooks) query.getSingleResult();
        } catch (NoResultException e) {
            borrowedBooks = null;
        }

        transaction.commit();
        session.close();

        return borrowedBooks;
    }

    @Override
    public BorrowedBooks returnBook(Integer memberId, String memberName, String title, String authorName, String returnDate) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = findBorrowedBook(memberId, memberName, title, authorName).getId();
        session.saveOrUpdate(findBorrowedBook(memberId, memberName, title, authorName));

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public BorrowedBooks update(BorrowedBooks entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.saveOrUpdate(entity);

        transaction.commit();
        session.close();

        return findById(id);
    }

    @Override
    public boolean delete(BorrowedBooks entity) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Integer id = entity.getId();
        session.delete(entity);

        transaction.commit();
        session.close();

        return findById(id) == null;
    }

    @Override
    public BorrowedBooks findById(Integer id) {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Query query = session.createQuery("from BorrowedBooks where id=:id");
        query.setParameter("id", id);

        BorrowedBooks borrowedBooks;

        try {
            borrowedBooks = (BorrowedBooks) query.getSingleResult();
        } catch (NoResultException e) {
            borrowedBooks = null;
        }

        transaction.commit();
        session.close();

        return borrowedBooks;
    }

    @Override
    public List<BorrowedBooks> findAll() {
        SessionFactory sessionFactory = HibernateConfiguration.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        // Native SQL - not preferred
//         Query query = session.createSQLQuery("select * from user");

        TypedQuery<BorrowedBooks> query = session.getNamedQuery("findAllBorrowedBooks");
        List<BorrowedBooks> borrowedBooks = query.getResultList();

        transaction.commit();
        session.close();

        return borrowedBooks;
    }

}