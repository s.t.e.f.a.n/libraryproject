package app.repository;

import app.model.Authors;

public interface AuthorsRepository extends CRUDRepository<Authors, Integer> {

    Authors findAuthorByName(String name);

}