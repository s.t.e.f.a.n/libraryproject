package app.repository;

import app.model.Books;

public interface BooksRepository extends CRUDRepository<Books,Integer> {

    Books addBook(String title, String authorName, String genresName, String isbn, Integer nrCopii);

    Books findBookByTitle(String name);

    Books findBookByTitleAndAuthor(String title, String authorName);

    boolean removeBook(String title, String authorName);

}